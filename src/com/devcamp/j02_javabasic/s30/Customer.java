package com.devcamp.j02_javabasic.s30;

import java.security.PublicKey;
import java.util.concurrent.Flow.Publisher;

import javax.print.DocFlavor.BYTE_ARRAY;

public class Customer {
    byte myByte;
    short myShortNumber;
    int myIntNumber;
    long myLongNumber;
    float myFloatNumber;
    double myDoubleNumber;
    boolean myBooleanNumber;
    char myCharNumber;

    public Customer() {
         myByte = 127;
         myShortNumber = 2021;
         myIntNumber = 6;
         myLongNumber = 2022;
         myFloatNumber = 5.66f;
         myDoubleNumber = 121.3;
         myBooleanNumber = false;
         myCharNumber = 'P'; 
    }

    public Customer(byte byteNum, short shortNum, int intNum, long longNum, float floatNum,
                    double doubleNum, boolean booleanNum, char charNum) {
        myByte = byteNum;
        myShortNumber = shortNum;
        myIntNumber = intNum;
        myLongNumber = longNum;
        myFloatNumber = floatNum;
        myDoubleNumber = doubleNum;
        myBooleanNumber = booleanNum;
        myCharNumber = charNum; 
    }

    public static void main(String[] args){
        Customer customer = new Customer();
        System.out.println(customer.myByte);
        System.out.println(customer.myShortNumber);
        System.out.println(customer.myIntNumber);
        System.out.println(customer.myLongNumber);
        System.out.println(customer.myFloatNumber);
        System.out.println(customer.myDoubleNumber);
        System.out.println(customer.myBooleanNumber);
        System.out.println(customer.myCharNumber);

        System.out.println("Run 1");
        byte myByte = 10;
        short myShort = 2022;
        Customer customer1 = new Customer(myByte,myShort,120, 6001, 71.7f, 123.9, false, 'A');
        System.out.println(customer1.myByte);
        System.out.println(customer1.myShortNumber);
        System.out.println(customer1.myIntNumber);
        System.out.println(customer1.myLongNumber);
        System.out.println(customer1.myFloatNumber);
        System.out.println(customer1.myDoubleNumber);
        System.out.println(customer1.myBooleanNumber);
        System.out.println(customer1.myCharNumber);

        System.out.println("Run 2");
        myByte = 20;
        myShort = 2023;
        Customer customer2 = new Customer(myByte,myShort,121, 6002, 71.1f, 124.9, true, 'B');
        System.out.println(customer2.myByte);
        System.out.println(customer2.myShortNumber);
        System.out.println(customer2.myIntNumber);
        System.out.println(customer2.myLongNumber);
        System.out.println(customer2.myFloatNumber);
        System.out.println(customer2.myDoubleNumber);
        System.out.println(customer2.myBooleanNumber);
        System.out.println(customer2.myCharNumber);

        System.out.println("Run 3");
        myByte = 30;
        myShort = 2024;
        Customer customer3 = new Customer(myByte,myShort,122, 6003, 70.1f, 125.9, false, 'C');
        System.out.println(customer3.myByte);
        System.out.println(customer3.myShortNumber);
        System.out.println(customer3.myIntNumber);
        System.out.println(customer3.myLongNumber);
        System.out.println(customer3.myFloatNumber);
        System.out.println(customer3.myDoubleNumber);
        System.out.println(customer3.myBooleanNumber);
        System.out.println(customer3.myCharNumber);

        System.out.println("Run 4");
        myByte = 40;
        myShort = 2025;
        Customer customer4 = new Customer(myByte,myShort,123, 6004, 69.1f, 126.9, false, 'D');
        System.out.println(customer4.myByte);
        System.out.println(customer4.myShortNumber);
        System.out.println(customer4.myIntNumber);
        System.out.println(customer4.myLongNumber);
        System.out.println(customer4.myFloatNumber);
        System.out.println(customer4.myDoubleNumber);
        System.out.println(customer4.myBooleanNumber);
        System.out.println(customer4.myCharNumber);

        System.out.println("Run 5");
        myByte = 50;
        myShort = 2026;
        Customer customer5 = new Customer(myByte,myShort,124, 6005, 68.1f, 127.9, true, 'E');
        System.out.println(customer5.myByte);
        System.out.println(customer5.myShortNumber);
        System.out.println(customer5.myIntNumber);
        System.out.println(customer5.myLongNumber);
        System.out.println(customer5.myFloatNumber);
        System.out.println(customer5.myDoubleNumber);
        System.out.println(customer5.myBooleanNumber);
        System.out.println(customer5.myCharNumber);
    }
}


